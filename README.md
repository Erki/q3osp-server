# q3osp server

## Install docker
Arch/Manjaro example
```sh
sudo pacman -S docker docker-compose
sudo systemctl start docker.service
sudo systemctl enable docker.service
```
Optionally add your user to `docker` group so you can run docker commands without `sudo`
```sh
sudo usermod -aG docker <username>
```

## Set up ioq3 server
* Copy your `pak0.pk3` to `./baseq3` folder (not provided in this repository)
* Configure `server.cfg` and `maps.cfg`

## Running/stopping the server
* `docker-compose up` starts the server / `Ctrl+c` stops
* `docker-compose up -d` to start as daemon in background / `docker-compose down` stops
* `docker-compose ps` shows all running docker processes
* `docker-compose restart` restarts server. Use when making changes to cfg files
* `docker-compose -h` for more options

## Extra
* Uses 27960 port, configurable in `docker-compose.yml`. For example to use 27969:
```yml
    ...
    ports:
      - "0.0.0.0:27960:27969/udp"
    ...
```
* When using provided docker image, no building is required. If there is an update to ioq3, OSP or you want to add other mods, edit `Dockerfile`, comment out the `image` line and uncomment `build`. Then run `docker-compose build`
