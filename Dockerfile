FROM alpine:latest as builder

# 1. Installs make dependencies
# 2. Gets ioq3 repo, q3 pk3 assets, OSP files
# 3. Builds ioq3
# 4. Copies pk3 files and OSP
RUN \
  apk --no-cache add curl g++ gcc git make && \
  mkdir -p /tmp/build && \
  curl https://raw.githubusercontent.com/ioquake/ioq3/master/misc/linux/server_compile.sh -o /tmp/build/compile.sh && \
  curl https://ioquake3.org/data/quake3-latest-pk3s.zip --referer https://ioquake3.org/extras/patch-data/ -o /tmp/build/quake3-latest-pk3s.zip && \
  curl http://osp.dget.cc/orangesmoothie/downloads/osp-Quake3-1.03a_full.zip -o /tmp/build/osp-Quake3-1.03a_full.zip && \
  echo "y" | sh /tmp/build/compile.sh && \
  unzip /tmp/build/quake3-latest-pk3s.zip -d /tmp/build/ && \
  cp -r /tmp/build/quake3-latest-pk3s/* ~/ioquake3 && \
  unzip /tmp/build/osp-Quake3-1.03a_full.zip -d /tmp/build/ && \
  cp -r /tmp/build/osp ~/ioquake3

FROM alpine:latest 
RUN adduser ioq3srv -D
COPY --from=builder /root/ioquake3 /home/ioq3srv/ioquake3
USER ioq3srv
